# GLEAM-300MHz configs

Configuration files for the GLEAM 300-MHz project for calibration/imaging/pre-calibration. Intended to be used with [piip](https://gitlab.com/Sunmish/piip).


