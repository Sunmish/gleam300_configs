#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --time=00:20:00
#SBATCH --cpus-per-task=4
#SBATCH --mem=16GB
#SBATCH --account=pawsey0272
#SBATCH --export=ALL
#SBATCH --output=/scratch/mwasci/duchesst/processing/logs/sf.%x.o%A

SCALE=0.5

PIIPPATH=/software/projects/mwasci/duchesst/piip/

container="singularity run -B /software/,/scratch/ /software/projects/mwasci/duchesst/piip.img"
config=/scratch/mwasci/duchesst/gleam300_configs/gleam_235_image.cfg

. $config

chan=235
scale=$(echo "${SCALE}/${chan}" | bc -l)

image=$1

current_imsize=$($container ${PIIPPATH}/get_header_key.py ${image} NAXIS1)
ra=$($container ${PIIPPATH}/get_header_key.py ${image} CRVAL1)
dec=$($container ${PIIPPATH}/get_header_key.py ${image} CRVAL2)
maxsep=$(echo "${scale}*10" | bc -l)
bmaj=$($container ${PIIPPATH}/get_header_key.py ${image} "BMAJ")
zone=$(echo "${bmaj}*2" | bc -l)

radius=$(echo "${scale}*${current_imsize}/2.0 - 0.5" | bc -l)

$container ${PIIPPATH}/piipaegean.sh ${image} ${ra} ${dec} ${radius} ${config}

imageCatalogue=${image/.fits/_masked_comp.vot}
raKey="ra"
decKey="dec"
fluxKey="int_flux"
pfluxKey="peak_flux"
efluxKey="err_int_flux"
epfluxKey="err_peak_flux"
fluxKeys="int_flux peak_flux"
rmsKey="local_rms"

if [ -z "${MATCH_CATALOGUES_OPTIONS}" ]; then
    MATCH_CATALOGUES_OPTIONS="--ra2 ra --dec2 dec"
fi

$container match_catalogues ${imageCatalogue} ${SKYMODEL} \
    --separation ${maxsep} \
    --exclusion_zone ${zone} \
    --threshold 0.0 \
    --nmax 800 \
    --outname ${image/.fits/}_matched.fits \
    --coords ${ra} ${dec} \
    --radius ${radius} \
    --ra1 ${raKey} \
    --dec1 ${decKey} \
    --flux_key ${fluxKey} \
    --eflux_key ${efluxKey} \
    -f1 ${fluxKeys} \
    --localrms ${rmsKey} \
    --ratio 1.2 \
    ${MATCH_CATALOGUES_OPTIONS}

${container} ${PIIPPATH}/quick_look.py "${image}" \
    --cmap cubehelix \
    --inset \
    --rms

rm ${image/.fits/_rms.fits}
rm ${image/.fits/_bkg.fits}